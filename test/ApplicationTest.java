import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import models.Customer;
import org.junit.*;
import static org.junit.Assert.*;

import play.data.Form;
import play.mvc.*;
import play.test.*;
import play.data.DynamicForm;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.RequiredValidator;
import play.i18n.Lang;
import play.libs.F;
import play.libs.F.*;
import play.twirl.api.Content;

import static play.data.Form.form;
import static play.test.Helpers.*;



/**
*
* Simple (JUnit) tests that can call all parts of a play app.
* If you are interested in mocking a whole application, see the wiki for more details.
*
*/
public class ApplicationTest {

    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertEquals(2, a);
    }

    @Test
    public void renderTemplate() {
        //requires static import of static play.data.Form.form;
//        final  Form<Customer> customerForm = form(Customer.class);
//        Content html = (play.twirl.api.Content) views.html.index.render("oleose", "subtitle", customerForm);
//       assertEquals("text/html", contentType(html));
//        assertTrue(contentAsString(html).contains("oleose"));
    }


}
