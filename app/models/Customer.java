package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.*;

/**
 * Created by Adam on 6/29/2015.
 */
@Entity
public class Customer extends Model{

    private static Model.Find<String, Customer> find = new Model.Finder<>(Customer.class);

    public String name;
    public String email;
    public String phone;
    public String message;

    public Customer() {
    }

    public Customer(String name, String email, String phone, String message) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.message = message;
    }

    public static List<Customer> findAll() {
        return Customer.find.orderBy("name").findList();
    }

}
