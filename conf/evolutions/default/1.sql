# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table customer (
  name                      varchar(255),
  email                     varchar(255),
  phone                     varchar(255),
  message                   varchar(255))
;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists customer;

SET REFERENTIAL_INTEGRITY TRUE;

